package exercice3;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RSSReader {
	public static InputStream read(String url) throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();

		HttpGet get = new HttpGet(url);
		HttpResponse response = httpClient.execute(get);
		StatusLine statusLine = response.getStatusLine();
		int statusCode = statusLine.getStatusCode();
		if (statusCode != HttpStatus.SC_OK) {
			System.err.println("Method failed : " + statusLine);
		}
		InputStream inputStream = response.getEntity().getContent();
		return inputStream;
	}

	public static class DefaultHandlerExtension extends DefaultHandler {
		private boolean root = true, z1 = false;
		private String title = new String();
		private String secTitle = new String();
		private String data = new String();
		private List<String> cate;
		private int comptesn = 0;
		private int compt = 0;
		private int comptitem = 0;
		private List<String> titre;

		public void startDocument() throws SAXException {
			cate = new ArrayList<String>();
			titre = new ArrayList<String>();

		}

		public void startElement(String uri, String localName, String qName, Attributes atr) throws SAXException {
			if (uri == "http://purl.org/dc/elements/1.1/") {
				comptesn++;

			}
			if (root) {
				System.out.println("Le nom de la premiere root est :" + qName);
				root = false;
				//
			} else
				compt++;
			if (qName == "item") {
				comptitem++;
				z1 = true;

			}

		}

		public void endElement(String uri, String localName, String qName) throws SAXException {

			if (qName == "title") {
				title = data;
			}

			if (z1) {
				if (qName == "title") {
					titre.add(data);
				}
			}
			if (qName == "item") {
				z1 = false;
			}
			if (qName == "cate") {
				cate.add(data);
			}
		}

		public void characters(char[] caracteres, int debut, int longueur) {
			data = new String(caracteres, debut, longueur);

			if (data.startsWith("Microservices")) {

				secTitle = title;
			}

		}

		public int getcpt() {
			return compt;
		}

		public int getitem() {
			return comptitem;
		}

		public List<String> getitre() {
			return titre;
		}

		public List<String> getcategory() {
			return cate;
		}

		public int getcptesn() {
			return comptesn;
		}

		public String gettimicro() {
			return secTitle;
		}

	}

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, Exception {
		// TODO Auto-generated method stub
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		saxParserFactory.setNamespaceAware(true);
		saxParserFactory.setValidating(true);
		SAXParser saxParser = saxParserFactory.newSAXParser();

		String url = "https://feed.infoq.com/";

		DefaultHandlerExtension h = new DefaultHandlerExtension();

		saxParser.parse(read(url), h);

		System.out.println("Le nbre de s-elts: " + h.getcpt());
		System.out.println("Le nbre des items: " + h.getitem());
		List<String> ti = h.getitre();
		System.out.println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");

		System.out.println("La liste des titres: ");
		for (String t : ti) {
			System.out.println(t);
		}
		List<String> cat = h.getcategory();

		System.out.println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
		System.out.println("La liste cat�gories: ");

		for (String c : cat) {
			System.out.println(c);
		}

		System.out.println("\n");
		System.out.println("Le nbre d�elts attach�s au nom : " + h.getcptesn());

		System.out.println("\n");
		System.out.println("le nbre du titre de microservice : " + h.gettimicro());

	}
}
