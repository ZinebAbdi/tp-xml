package exercice1et2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class PlayWithXml {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		User user = new User(1, "NGolo", 28);
		System.out.println("user= " + user);

		Document document = DocumentHelper.createDocument();

		Element rootElement = document.addElement("user");
		rootElement.addAttribute("id", "" + user.getId());

		Element nameElement = rootElement.addElement("name");
		nameElement.addText(user.getName());

		Element ageElement = rootElement.addElement("age");
		ageElement.setText("" + user.getAge());

		try (Writer writer = new FileWriter("xml/user.xml");) {
			OutputFormat format = OutputFormat.createPrettyPrint();
			XMLWriter xmlWriter = new XMLWriter(writer, format);
			xmlWriter.write(document);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Exo2: Rajouter une m�thode read
	public Document read(String file) {
		File fichier = new File(file);

		Document doc = DocumentHelper.createDocument();

		SAXReader reader = new SAXReader();

		try {
			doc = reader.read(fichier);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		return doc;

	}

	// Exo2: Cr�er une m�thode deserialize()
	public User deserialize(Document doc) {
		User M = new User();

		Element root = doc.getRootElement();
		List<Element> elements = root.elements();
		List<Attribute> attributs = root.attributes();

		M.id = (int) Long.parseLong(attributs.get(0).getValue());
		M.name = elements.get(0).getStringValue();
		M.age = Integer.parseInt(elements.get(2).getStringValue());

		return M;
	}
}
